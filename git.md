# git 命令
git config --global user.name '' 设置用户名
git config --global user.email ''   邮件地址
git config --global replace-all user.email '' 修改
git config --list 查看设置


初始化项目 使用命令
git init

//(master)当前所处分支
git status  查看状态
git add 添加到暂存区
git add . 把所有的文件都添加进去
git rm --cached +名字撤销 回到上一个状态

# 将暂存区 提交到版本库
git commit -m '' 提交信息
git commit -am  '' 提交到版本库省略了add 新增文件不会提交


git log 查看提交历史
git log --graph 图形化查看提交信息 可以查看分支

git diff 显示变更内容 包括新增内容 删除了哪些内容 提交之后不会显示变化
git diff + 文件名 只显示该文件变更
q 推出

git  reset HEAD n' 回退版本

clear 清屏

git reset --hard +id 回退到id的代码 覆盖日志
git revert name 撤销 会有新日志 不会覆盖日志

git revert

# 分支
git branch 显示本地所有分支 以及当前所在哪个分支
git branch  name创建新分支
git branch -d name 删除分支
git checkout  mame 切换分支
git checkout -b mame 创建并切换分支

git merge name 合并分支 注意当前分支

rm rf name删除文件夹


拉取远程分支到本地合并
 git pull origin master 

 git log --oneline --decorate 简单显示 一行输出 高亮
 git log --oneline --decorate --graph  简单显示 分支

git reflog 查看日志 简化

git tag v1.0 添加版本号 +name 给id添加版本号
git tag -d v 删除版本号
git show  显示提交信息

# 远程仓库
gitlab 

GitHub 免费开源

码云 gitee
git remote add origin http://''

推送本地仓库代码到远程仓库
git push origin master

# 密钥

ssh-keygen